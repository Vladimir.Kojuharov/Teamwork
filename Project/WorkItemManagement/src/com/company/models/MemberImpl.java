package com.company.models;

import com.company.models.contracts.Member;
import com.company.models.contracts.Person;
import com.company.models.contracts.Workitem;
import com.company.models.contracts.ListContainer;

import java.util.List;

public class MemberImpl implements Member {

    private String name;
    private List<Workitem> items;          //collection
    private List<Activities> activities;    // collection


    public MemberImpl(String name, List<Workitem> items, List<Activities> activities) {
        setName(name);
        this.items = items;
        this.activities = activities;
    }









    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.length() < 5 || name.length() > 15 || name == null) {
            throw new IllegalArgumentException("Name should be unique string between 5 and 15 symbols!");
        }
        this.name = name;
    }


    @Override
    public List<Workitem> getListOfItems() {
        return items;
    }

    public void setItems(List<Workitem> items) {
        this.items = items;
    }



    @Override
    public List<Activities> getListOfActivities() {
        return activities;
    }

    public void setActivities(List<Activities> activities) {
        this.activities = activities;
    }





}
