package com.company.models.enums;

public enum StoryStatusType {
    NotDone,
    InProgress,
    Done
}
