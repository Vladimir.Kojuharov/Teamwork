package com.company.models.enums;

public enum FeedbackStatusType {
    New,
    Unscheduled,
    Scheduled,
    Done
}
