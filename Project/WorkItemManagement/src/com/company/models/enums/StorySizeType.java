package com.company.models.enums;

public enum StorySizeType {
    Large,
    Medium,
    Small
}
