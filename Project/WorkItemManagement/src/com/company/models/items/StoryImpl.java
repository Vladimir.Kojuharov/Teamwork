package com.company.models.items;

import com.company.models.contracts.History;
import com.company.models.contracts.Story;
import com.company.models.enums.PriorityType;
import com.company.models.enums.StorySizeType;

public class StoryImpl extends WorkItemBase implements Story {

    private StorySizeType typeOfSize;

    public StoryImpl(int iD, String title, String description, TypeOfStatus status, History history,
                     PriorityType priority, Person assignee, StorySizeType typeOfSize) {

        super(iD, title, description, status, history, priority, assignee);
        this.typeOfSize = typeOfSize;
    }


    @Override
    public StorySizeType getSize() {
        return typeOfSize;
    }


}
