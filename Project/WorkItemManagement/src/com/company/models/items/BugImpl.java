package com.company.models.items;

import com.company.models.contracts.Bug;
import com.company.models.contracts.History;
import com.company.models.enums.BugSeverityType;
import com.company.models.enums.PriorityType;

import java.util.List;

public class BugImpl extends WorkItemBase implements Bug {

    private List<String> stepsOfReproduce;
    private BugSeverityType bugSeverityType;

    public BugImpl(int iD, String title, String description, TypeOfStatus status, History history, PriorityType priority,
                   Person assignee, List<String> stepsOfReproduce, BugSeverityType bugSeverityType) {

        super(iD, title, description, status, history, priority, assignee);
        this.stepsOfReproduce = stepsOfReproduce;
        this.bugSeverityType = bugSeverityType;
    }


    @Override
    public List<String> getStepsOfReproduce() {
        return null;
    }

    public void setStepsOfReproduce(List<String> stepsOfReproduce) {
        this.stepsOfReproduce = stepsOfReproduce;
    }


    @Override
    public BugSeverityType getBugSeverityType() {
        return bugSeverityType;
    }


}
