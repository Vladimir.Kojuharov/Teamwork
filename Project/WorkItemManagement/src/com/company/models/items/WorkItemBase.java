package com.company.models.items;


import com.company.models.contracts.Comment;
import com.company.models.contracts.History;
import com.company.models.contracts.Person;
import com.company.models.contracts.Workitem;
import com.company.models.enums.PriorityType;

import java.awt.*;
import java.util.ArrayList;

public class WorkItemBase implements Workitem {
    private int iD;
    private String title;
    private String description;
    private List<Comment> comments; //TODO --> comentarite shte se dobavqt v posledstvie, sled syzdaaneto na obekta (za tova ne sa v konstruktora)
    private TypeOfStatus status; //TODO --> koi taip of status
    private History history;

    private PriorityType priority;
    private Person assignee;


    //Feedback Construktor
    public WorkItemBase(int iD, String title, String description, TypeOfStatus status, History history) {
        this.iD = iD;
        setTitle(title);
        setDescription(description);
        this.TypeOfStatus = status;
        this.History = history;
    }

    //Bug & Stoory Construktor
    public WorkItemBase(int iD, String title, String description, TypeOfStatus status, History history, PriorityType priority, Person assignee) {
        this.iD = iD;
        setTitle(title);
        setDescription(description);
        this.TypeOfStatus = status;
        this.history = history;
        this.priority = priority;
        this.Person = assignee;
    }


    @Override
    public int getID() {
        return iD;
    }

    private void setiD(int iD) {    // set ?? --> automatic generate Set?
        this.iD = iD;
    }

    @Override
    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        if (title.length() < 10 || title.length() > 50 || title == null) {
            throw new IllegalArgumentException("Title must be between 10 and 50 symbols and can't be null");
        }
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        if (description.length() < 10 || description.length() > 500 || description == null) {
            throw new IllegalArgumentException("Description must be between 10 and 500 symbols and can't be null");
        this.description = description;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

    private void setComments(List<Comment> comments) {
        this.comments = comments;
    }


    @Override
    public TypeOfStatus getTypeOfStatus() {
        return status;
    }


    @Override
    public List<History> getHistory() {
        return history;
    }

    private void setHistory(History history) {
        this.history = history;
    }

    public PriorityType getPriority() {
        return priority;
    }

    public Person getAssignee() {
            //TODO validation for a person -> being a valid member of the team -> "Assignee is a member from the team"
        }
        return assignee;
    }


}
