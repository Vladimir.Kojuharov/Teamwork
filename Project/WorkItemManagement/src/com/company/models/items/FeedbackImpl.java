package com.company.models.items;

import com.company.models.contracts.Feedback;
import com.company.models.contracts.History;

public class FeedbackImpl extends WorkItemBase implements Feedback {

    private int rating;


    public FeedbackImpl(int iD, String title, String description, TypeOfStatus status, History history, int rating) {
        super(iD, title, description, status, history);
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    private void setRating(int rating) {
        this.rating = rating;
    }
}
