package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Person;
import com.company.models.contracts.Team;

import java.util.List;

public class TeamImpl implements Team {

    private String name;
    private List<Person> person;
    private List<Board> board;



    public TeamImpl(String name, List<Person> person, List<Board> board) {
        this.name = name;
        this.person = person;
        this.board = board;
    }



    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }


    @Override
    public List<Person> getMembers() {
        return person;
    }

    private void setPerson(List<Person> person) {
        this.person = person;
    }


    @Override
    public List<Board> getBoards() {
        return board;
    }

    private void setBoard(List<Board> board) {
        this.board = board;
    }




}
