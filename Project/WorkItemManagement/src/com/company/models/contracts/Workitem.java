package com.company.models.contracts;

import com.company.models.enums.PriorityType;

import java.util.List;

public interface Workitem {

    int getID();
    String getTitle();
    String getDescription();
    List<Comment> getComments();     //Comments is a list of comments (string messages with author).
    TypeOfStatus getTypeOfStatus();  //Status is one of the following: Active, Fixed // NotDone, InProgress, Done // New, Unscheduled, Scheduled, Done
    List<String> getHistory();       //History is a list of all changes (string messages) that were done to the subclass.
    PriorityType getPriority();


}
