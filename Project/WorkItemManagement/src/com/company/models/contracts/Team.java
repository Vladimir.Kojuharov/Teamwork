package com.company.models.contracts;

public interface Team  {

    String getName();
    List<Person> getMembers();
    List<Board> getBoards();




}
