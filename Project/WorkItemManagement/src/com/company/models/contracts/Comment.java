package com.company.models.contracts;

public interface Comment {
    String getComment();
}
