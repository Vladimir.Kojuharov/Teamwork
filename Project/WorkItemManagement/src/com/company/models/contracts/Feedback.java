package com.company.models.contracts;

public interface Feedback extends Workitem {

    int getRating();

}
