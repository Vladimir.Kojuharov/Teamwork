package com.company.models.contracts;

import java.util.List;

public interface Board {

    String getName();
    List<Workitem> getWorkitems();
    List<Activities> getActivities();

}
