package com.company.models.contracts;

import com.company.models.enums.StorySizeType;

public interface Story {

    StorySizeType getSize();

}
