package com.company.models.contracts;

import com.company.models.enums.BugSeverityType;

import java.util.List;

public interface Bug extends Workitem {

       List<String> getStepsOfReproduce();
       BugSeverityType getBugSeverityType();
}
