package com.company.models.contracts;

import java.util.List;

public interface Member extends Person {

    @Override
    String getName();
    List<Workitem> getListOfItems();
    List<Activities> getListOfActivities();

}
