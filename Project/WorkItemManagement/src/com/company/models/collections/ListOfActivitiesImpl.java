package com.company.models.collections;

import com.company.models.contracts.ListContainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfActivitiesImpl implements ListContainer {

    private List<String> activities = new ArrayList<>();

    //TODO
    //List(<String>)
    // getActivity() String
    // addActivity(String)
    // removeActivity(String)

    @Override
    public List<String> getList() {
        return Collections.unmodifiableList(activities);
    }
}