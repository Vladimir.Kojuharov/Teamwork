package com.company.models.collections;

import com.company.models.contracts.ListContainer;
import com.company.models.contracts.Workitem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfItemsImpl implements ListContainer {

    private List<Workitem> items = new ArrayList<>();

    //List<item> workItems

    //getItem() item
    //addItem(item)
    //removeItem(item)

    @Override
    public List<Workitem> getList() {
        return Collections.unmodifiableList(items);
    }







}
