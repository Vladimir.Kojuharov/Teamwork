package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Workitem;
import com.sun.corba.se.spi.orbutil.threadpool.Work;

import java.util.List;

public class BoardImpl implements Board {

    private String name;
    private List<Workitem> items;          //collection
    private List<Activities> activities;    // collection


    public BoardImpl(String name, List<Workitem> items, List<Activities> activities) {
        setName(name);
        this.items = items;
        this.activities = activities;
    }


    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {//TODO - name must be Unique -->   foreach...  if (name.equals ....i..) {throw new IllegalArgumentException("Name should be unique string between 5 and 10 symbols!");}
        if (name.length() < 5 || name.length() > 15 || name == null) {
            throw new IllegalArgumentException("Name should be unique string between 5 and 10 symbols!");
        }
        this.name = name;
    }


    public List<Workitem> getItems() {
        return items;
    }

    private void setItems(List<Workitem> items) {
        this.items = items;
    }


    @Override
    public List<Activities> getActivities() {
        return activities;
    }

    private void setActivities(List<Activities> activities) {
        this.activities = activities;
    }


}
